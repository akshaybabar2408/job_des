from __future__ import unicode_literals

from django.db import models


class Place(models.Model):
	lat = models.CharField(max_length=100)
	lon = models.CharField(max_length=100)
	address = models.CharField(max_length=200)

	def __str__(self):
		return "{0} :: {1} :: {2}".format(self.lat, self.lon, self.address)
