from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^dashboard/$', views.dashboard, name='home'),
    url(r'^create_place/$', views.create_place, name='create_place'),
    url(r'^clear_database/$', views.clear_database, name='clear_database'),
]
