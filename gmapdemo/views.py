from django.shortcuts import render, redirect
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt

from .models import Place


def dashboard(request):
    places = Place.objects.all()
    return render(request, 'dashboard.html', {'places':places})

@csrf_exempt
def create_place(request):
    try:
        place = Place.objects.create( address = request.POST['address'],
                                       lat = request.POST['lat'],
                                       lon = request.POST['lng']
                                    )
        data = { 'status': 201, 'address': place.address, 'lat': place.lat, 'lon': place.lon }
    except Exception as e:
        data = { 'status': 500, 'errors': 'Something went wrong!' }

    return JsonResponse(data)


def clear_database(request):
    Place.objects.all().delete()
    return redirect('/dashboard')
